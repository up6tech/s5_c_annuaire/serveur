//
// Created by Dorian Gardes on 13/01/2021.
//

#ifndef SERVEUR_REQADMIN_H
#define SERVEUR_REQADMIN_H

#include <stdbool.h>

/**************************************************
 * 												  *
 *           ADMINISTRATION DES COMPTES           *
 * 												  *
 **************************************************/


/**
 * Création des fichiers d'un compte
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 03/01/2021
 * @param nom Nom du compte
 * @param motDePasse
 * @return True si compté créé avec succès. Faux sinon.
 */
bool ADMIN_creerCompte(const char* nom, const char* motDePasse);

/**
 * Destruction des fichiers d'un compte
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 04/01/2021*
 * @param idCompte id du compte à modifier (l'id est le numéro affiché à coté d'un utilisateur sur le client)
 * @return True si compté détruit avec succès. Faux sinon.
 */
bool ADMIN_detruireCompte(const char* idCompte);

/**
 * Modifie le nom ou le mot de passe d'un compte donné
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 04/01/2021
 * @param idCompte id du compte à modifier (l'id est le numéro affiché à coté d'un utilisateur sur le client)
 * @param nomDuChamp Valeurs possibles : "nom" ou "motdepasse"
 * @param nouvelleValeur Valeur à modifier
 * @return True si modification réussie. Faux sinon.
 */
bool ADMIN_modifierCompte(const char* idCompte, const char* nomDuChamp, const char* nouvelleValeur);

/**
 * Liste les comptes disponibles sur ce serveur
 * Si le retour est "NULL" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 04/01/2021
 * @return La liste des comptes utilisateurs
 */
char* ADMIN_afficherComptes();

#endif //SERVEUR_REQADMIN_H

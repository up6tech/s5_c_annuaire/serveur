//
// Created by Dorian Gardes on 13/01/2021.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <malloc.h>
#include <dirent.h>
#include <math.h>
#include <errno.h>

#include "reqAdmin.h"
#include "../aiguillageServeur.h"
#include "../utils.h"


/**************************************************
 * 												  *
 *           ADMINISTRATION DES COMPTES           *
 * 												  *
 **************************************************/

/**
 * Création des fichiers d'un compte
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 03/01/2021
 * @param nom Nom du compte
 * @param motDePasse
 * @return True si compté créé avec succès. Faux sinon.
 */
bool ADMIN_creerCompte(const char* nom, const char* motDePasse) {

	// Protection supplémentaire pour ne pas avoir deux comptes admin.
	if (strcmp(nom, "admin") == 0) {
		envoyerReponse(401, "Opération non permise: compte admin protégé.");
		return false;
	}

	FILE* fichier = NULL;
	if (ouvrirFichier(nom, &fichier, "r")) {
		fclose(fichier);
		envoyerReponse(402, "Utilisateur déjà existant");
		return false;

	} else if (ouvrirFichier(nom, &fichier, "w")) {
		char* hash = sha512(motDePasse);
		fprintf(fichier, "%s\n", hash);
		free(hash);
		fclose(fichier);
		return true;
	} else {
		envoyerReponse(500, "Erreur à la création su compte");
		return false;
	}

}

/**
 * Destruction des fichiers d'un compte
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 04/01/2021
 * @param idCompte id du compte à modifier (l'id est le numéro affiché à coté d'un utilisateur sur le client)
 * @return True si compté détruit avec succès. Faux sinon.
 */
bool ADMIN_detruireCompte(const char* idCompte) {

	char* nom = getNomCompteFromNumero(atoi(idCompte));
	if (!nom) return false;

	FILE* fichier = NULL;
	if (ouvrirFichier(nom, &fichier, "r")) { // Fichier existant
		fclose(fichier);

		// on Commence par le nettoyage des partages
		if (ouvrirFichier(".shares", &fichier, "r")) {

			// Etape 1: Connaitre la taille du fichier pour pouvoir le stocker.
			fseek(fichier, 0, SEEK_END);
			char* tampon = calloc(ftell(fichier) + 2, sizeof(char)); // On va y stocker temporairement le fichier
			rewind(fichier); // On repart au début

			// Etape 2: On lit le fichier et retire les partages
			char* nomEncode1 = encodeURI(nom);
			nomEncode1 = realloc(nomEncode1, (strlen(nomEncode1) +2) * sizeof(char));
			strcat(nomEncode1, " "); // On évite de supprimer les partages des comptes ressemblants
			char* nomEncode2 = strdup(nomEncode1);  // Par exemple le compte 'Pierre' ne va pas supprimer les partages
			nomEncode2[strlen(nomEncode2)] = '\n'; // du compte 'Pierre%20Andre' (pas de pb d'espaces etc car tout est encodé)

			char* ligne = NULL;
			size_t len = 0;
			while (getline(&ligne, &len, fichier) != -1) {

				// Si le nom du compte à supprimer n'est pas dans la ligne alors on la garde
				if (strstr(ligne, nomEncode1) == NULL && strstr(ligne, nomEncode2) == NULL) strcat(tampon, ligne);
			}
			free(nomEncode1);
			free(nomEncode2);

			// Etape 3: On ecrit dans le fichier sans les partages du compte a supprimer
			// On vide le fichier et récupère les droits d'écriture
			fichier = freopen(NULL, "w", fichier);
			fputs(tampon, fichier);

			fclose(fichier);
			free(tampon);

		} else {
			envoyerReponse(500, "Echec à l'ouverture des partages. Compte non supprimé");
			free(nom);
			return false;
		}

		// Partages correctements supprimés on retire le compte
		char* cheminFichier = getCheminFichier(nom);
		remove(cheminFichier); // On supprime le fichier
		free(cheminFichier);
		// Compte correctement supprimé
		free(nom);
		return true;
	} else {
		if (errno == ENOENT) envoyerReponse(404, "Utilisateur non trouvé");
		else envoyerReponse(500, "Erreur à l'ouverture du compte");
		free(nom);
		return false;
	}
}

/**
 * Modifie le nom d'un compte.
 * Méthode privée réservée à modifierCompte()
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 04/01/2021
 * @param idCompte id du compte à modifier (l'id est le numéro affiché à coté d'un utilisateur sur le client)
 * @param nouveauNom Valeur à modifier
 * @return True si modification réussie. Faux sinon.
 */
static bool modifierNomCompte(int idCompte, const char* nouveauNom) {

	char* nomCompte = getNomCompteFromNumero(idCompte);
	if (!nomCompte) return false;

	FILE* fichier;
	if (ouvrirFichier(nomCompte, &fichier, "r")) { // On vérifie que le compte existe
		fclose(fichier);

		// Etape 1: On renomme les partages
		if (ouvrirFichier(".shares", &fichier, "r")) {

			// Etape 1.1: Connaitre la taille du fichier pour pouvoir le stocker.
			fseek(fichier, 0, SEEK_END);
			long tailleFichier = ftell(fichier) +2;
			char* tampon = calloc(tailleFichier, sizeof(char)); // On va y stocker temporairement le fichier
			rewind(fichier); // On repart au début

			// Etape 1.2: On lit le fichier et renome les partages
			char* oldName = encodeURI(nomCompte);
			char* newName = encodeURI(nouveauNom);

			// Taille: 2 username encodés + espace central + \n + \0 + 2 de marge
			char* ligne = NULL;
			size_t len = 0;
			while (getline(&ligne, &len, fichier) != -1) {

				// Si le nom du compte à renommer n'est pas dans la ligne alors on touche pas
				if (strstr(ligne, oldName) == NULL) strcat(tampon, ligne);
				else {
					char* positionNom = strstr(ligne, oldName);

					// Si le nom est en première position && ce n'est pas une partie d'un nom composé
					if (positionNom == ligne && *(ligne + strlen(oldName)) == ' ') {

						char* secondNom = strchr(ligne, ' ');
						char* newLigne = calloc(strlen(newName) + strlen(secondNom) +2, sizeof(char));
						strcpy(newLigne, newName);
						strcat(newLigne, secondNom); // L'espace est sur le premier char de secondNom
						tampon = realloc(tampon, (tailleFichier - strlen(ligne) + strlen(newLigne) +1) * sizeof(char));
						strcat(tampon, newLigne);
						free(newLigne);

						// Le nom est en seconde position && ce n'est pas une partie d'un nom composé
					} else if (*(positionNom + strlen(oldName)) == '\n') {

						char* separateur = strchr(ligne, ' ');
						char* newLigne = calloc((separateur - ligne +1) + strlen(newName) +2, sizeof(char));
						strncpy(newLigne, ligne, separateur - ligne +1); // L'espace est sur le dernier char
						strcat(newLigne, newName);
						strcat(newLigne, "\n");
						tampon = realloc(tampon, (tailleFichier - strlen(ligne) + strlen(newLigne) +1) * sizeof(char));
						strcat(tampon, newLigne);
						free(newLigne);

					} else strcat(tampon, ligne);
				}
			}
			free(oldName);
			free(newName);

			// Etape 1.3: On ecrit dans le fichier la nouvelle version
			// On vide le fichier et récupère les droits d'écriture
			fichier = freopen(NULL, "w", fichier);
			fputs(tampon, fichier);

			fclose(fichier);
			free(tampon);

		} else {
			envoyerReponse(500, "Echec à l'ouverture des partages");
			return false;
		}


		// Etape 2: On renomme le compte
		char* oldPath = getCheminFichier(nomCompte);
		char* newPath = getCheminFichier(nouveauNom);
		rename(oldPath, newPath);
		free(oldPath);
		free(newPath);
		free(nomCompte);

		return true;
	}
	free(nomCompte);
	if (errno == ENOENT) envoyerReponse(404, "Utilisateur non trouvé");
	else envoyerReponse(500, "Erreur à l'ouverture du compte");
	return false;
}

/**
 * Modifie le mot de passe d'un compte.
 * Méthode privée réservée à modifierCompte()
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 04/01/2021
 * @param idCompte id du compte à modifier (l'id est le numéro affiché à coté d'un utilisateur sur le client)
 * @param nouveauMotDePasse Valeur à modifier
 * @return True si modification réussie. Faux sinon.
 */
static bool modifierMotDePasse(int idCompte, const char* nouveauMotDePasse) {

	char* nomCompte = getNomCompteFromNumero(idCompte);
	if (!nomCompte) return false;

	FILE* fichier = NULL;
	if (ouvrirFichier(nomCompte, &fichier, "r")) { // Fichier existant

		// Etape 1: Connaitre la taille du fichier pour pouvoir le stocker.
		fseek(fichier, 0, SEEK_END);
		size_t tailleFichier = ftell(fichier) +2;
		char* tampon = calloc(tailleFichier, sizeof(char)); // On va y stocker temporairement le fichier
		rewind(fichier); // On repart au début

		// Etape 2: On change le mot de passe
		getline(&tampon, &tailleFichier, fichier); // On lit l'ancien mot de passe (on ne va pas le garder)
		char* hashPswd = sha512(nouveauMotDePasse); // On hashe le nouveau mot de passe
		strcpy(tampon, hashPswd);
		free(hashPswd);

		// Etape 3: On lit le reste du fichier
		char* ligne = NULL;
		size_t len = 0;
		while (getline(&ligne, &len, fichier) != -1)
			strcat(tampon, ligne);

		// Etape 4: On ecrit la nouvelle version du fichier
		// On vide le fichier et récupère les droits d'écriture
		fichier = freopen(NULL, "w", fichier);
		fputs(tampon, fichier);

		fclose(fichier);
		free(tampon);
		free(nomCompte);
		return true;

	}
	free(nomCompte);
	if (errno == ENOENT) envoyerReponse(404, "Utilisateur non trouvé");
	else envoyerReponse(500, "Erreur à l'ouverture du compte");
	return false;
}

/**
 * Modifie le nom ou le mot de passe d'un compte donné
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 04/01/2021
 * @param idCompte id du compte à modifier (l'id est le numéro affiché à coté d'un utilisateur sur le client)
 * @param nomDuChamp Valeurs possibles : "nom" ou "motdepasse"
 * @param nouvelleValeur Valeur à modifier
 * @return True si modification réussie. Faux sinon.
 */
#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err34-c"
bool ADMIN_modifierCompte(const char* idCompte, const char* nomDuChamp, const char* nouvelleValeur) {

	int id = atoi(idCompte);
	// On modifie le nom du compte
	if (strcmp(nomDuChamp, "nom") == 0)
		return modifierNomCompte(id, nouvelleValeur);

		// On modifie le mot de passe du compte
	else if (strcmp(nomDuChamp, "motdepasse") == 0)
		return modifierMotDePasse(id, nouvelleValeur);

	else {
		char* msg = calloc(56 + strlen(nomDuChamp), sizeof(char));
		sprintf(msg, "Erreur de syntaxe: '%s' n'est pas un nom de champ valable", nomDuChamp);
		envoyerReponse(400, msg);
		free(msg);
		return false;
	}
}
#pragma clang diagnostic pop

/**
 * Liste les comptes disponibles sur ce serveur
 * Si le retour est "NULL" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 04/01/2021
 * @return La liste des comptes utilisateurs
 */
char* ADMIN_afficherComptes() {

	DIR* dossier = opendir(CHEMIN_BASE);
	if (dossier != NULL) {

		char* listeComptes = calloc(1, sizeof(char)); // On prépare un calloc de 1 au cas où il n'y ai pas de comptes sur le serveur.
		int i = 1;
		struct dirent* fichier;
		while ((fichier = readdir(dossier)) != NULL) {
			// Si c'est un dossier ou un fichier caché on saute
			#ifdef WIN32
				if ((strchr(fichier->d_name, '.')) == NULL || (fichier->d_name)[0] == '.') continue;
			#else
				if (fichier->d_type == 4 || (fichier->d_name)[0] == '.') continue;
			#endif

			char* ligne = calloc(strlen(fichier->d_name) + (int)log10(i) + 16, sizeof(char));
			sprintf(ligne, " \x1B[34m%d -> \x1B[0m %s\n", i, fichier->d_name);

			// On redimensionne dynamiquement la taille de la chaine
			#if defined(WIN32) || defined(__CYGWIN__)
				listeComptes = realloc(listeComptes, (strlen(listeComptes) + strlen(ligne) +2) * sizeof(char));
			#else
				listeComptes = reallocarray(listeComptes, strlen(listeComptes) + strlen(ligne) +2, sizeof(char));
			#endif
			strcat(listeComptes, ligne);
			free(ligne);
			i++;
		}
		closedir(dossier);
		if (strlen(listeComptes) == 0) {
			listeComptes = realloc(listeComptes, 46 * sizeof(char));
			strcpy(listeComptes, " \x1B[31m!   Pas de comptes sur ce serveur.\x1B[0m");
		}
		return listeComptes;

	} else {
		switch (errno) {
			case ENOENT:
				fprintf(stderr, "%s : Dossier introuvable\n", CHEMIN_BASE);
				break;
			case EACCES:
				fprintf(stderr, "%s : Permissions insuffisantes\n", CHEMIN_BASE);
				break;
			case ENOMEM:
				fprintf(stderr, "%s : Mémoire insuffisante pour ouvrir le dossier\n", CHEMIN_BASE);
				break;
			case ENOTDIR:
				fprintf(stderr, "%s : N'est pas un dossier\n", CHEMIN_BASE);
				break;
			default:
				fprintf(stderr, "%s : Impossible d'ouvrir le dossier, raison inconnue\n", CHEMIN_BASE);
				break;
		}
		envoyerReponse(500, "Echec à l'ouverture des comptes");
		return NULL;
	}
}

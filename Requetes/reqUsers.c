//
// Created by Dorian Gardes on 13/01/2021.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <errno.h>

#include "reqUsers.h"
#include "../aiguillageServeur.h"
#include "../utils.h"

/**************************************************
 * 												  *
 *     TRAITEMENT DES REQUETES UTILISATEURS       *
 * 												  *
 **************************************************/

/**
 * Création d'un contact dans un annuaire
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 14/01/2021
 * @param username Nom d'utilisateur du compte
 * @param tabArgs Tableau regroupant les différents champs d'un contact. Les champs factultatifs peuvent prendre la valeur NULL.
 * @return True si compté créé avec succès. Faux sinon.
 */
bool USER_creerContact(const char* username, const char** tabArgs) {

	FILE* fichier = NULL;
	if (ouvrirFichier(username, &fichier, "a+")) {

		for (int i = 0; i < 5; ++i) {
			if (tabArgs[i] != NULL) fprintf(fichier, "%s ", tabArgs[i]);
			else if (i != 4) { // On est sur la toute
				fprintf(fichier, " ");
			}
		}
		fprintf(fichier, "\n");
		fclose(fichier);
		return true;
	} else {
		if (errno == ENOENT) envoyerReponse(404, "Utilisateur non trouvé");
		else envoyerReponse(500, "Erreur à l'ouverture du compte");
		return false;
	}
}

/**
 * Création d'un partage d'annuaire
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 19/01/2021
 * @param username Nom d'utilisateur du compte
 * @param idBeneficiaire Id du bénéficiaire du partage
 * @return True si partage créé avec succès. Faux sinon.
 */
bool USER_creerPartage(const char* username, int idBeneficiaire) {

	FILE* fichier = NULL;
	if (ouvrirFichier(".shares", &fichier, "r")) {

		// Etape 1: On génère la ligne du partage
		char* nomBeneficiaire = getNomCompteFromNumero(idBeneficiaire);
		if (!nomBeneficiaire) return false;
		char* encUsername = encodeURI(username);
		char* encBeneficiare =encodeURI(nomBeneficiaire);
		free(nomBeneficiaire);

		// On évite de se partager son propre annuaire
		if (strcmp(encUsername, encBeneficiare) == 0) {
			envoyerReponse(401, "Opération non permise: impossible de partager son annuaire à soi même");
			return false;
		}

		char* partage = calloc(strlen(encUsername) + strlen(encBeneficiare) + 3, sizeof(char));
		sprintf(partage, "%s %s\n", encUsername, encBeneficiare);
		free(encUsername);
		free(encBeneficiare);

		// Etape 2: On vérifie que le partage ne sois pas déja existant
		char* ligneFichier = NULL;
		size_t len = 0; // Longueur du bloc mémoire "ligneFichier" utilisé uniquement pour getline()

		while (getline(&ligneFichier, &len, fichier) != -1) {

			if (strcmp(ligneFichier, partage) == 0) {
				envoyerReponse(402, "Partage déjà existant");
				return false; //Si le partage existe déja on sort.
			}

		}
		free(ligneFichier);

		// Etape 3: On ajoute le partage au fichier
		fichier = freopen(NULL, "a+", fichier);
		fputs(partage, fichier);
		free(partage);
		fclose(fichier);
		return true;
	} else {
		envoyerReponse(500, "Echec à l'ouverture des partages");
		return false;
	}
}

/**
 * Supprime le contact pointé par idContact
 * Si le retour est "false" la commande envoie la réponse au client
 * @param username Nom du compte courant
 * @param idContact Id du contact à supprimer
 * @return True si suppression réussie. Faux sinon.
 */
bool USER_detruireContact(const char* username, const char* idContact) {

	FILE* fichier;
	if (ouvrirFichier(username, &fichier, "r")) {

		// On cherche à connaitre la taille du fichier pour pouvoir le stocker.
		fseek(fichier, 0, SEEK_END);
		long tailleFichier = ftell(fichier) + 2;
		char* tampon = calloc(tailleFichier, sizeof(char)); // On va y stocker temporairement le fichier
		rewind(fichier); // On repart au début


		char* ligneFichier = NULL;
		size_t len = 0; // Longueur du bloc mémoire "ligneFichier" utilisé uniquement pour getline()
		int numDeLigne = 1;

		// On saute la première ligne qui contient le mot de passe du compte
		getline(&ligneFichier, &len, fichier);
		strcpy(tampon, ligneFichier); // On conserve le mot de passe

		while (getline(&ligneFichier, &len, fichier) != -1) {

			if (numDeLigne != atoi(idContact)) strcat(tampon, ligneFichier); //On n'est pas sur la bonne ligne on n'y touche pas

			numDeLigne ++;
		}
		free(ligneFichier);

		// Le fichier contient moins de lignes que le num de contact demandé
		if (numDeLigne - 1 < atoi(idContact)) {
			fclose(fichier);
			free(tampon);
			envoyerReponse(404, "Supression impossible : contact introuvable");
			return false;
		}

		// On ecrit la nouvelle version dans le fichier
		// On vide le fichier et récupère les droits d'écriture
		fichier = freopen(NULL, "w", fichier);
		fputs(tampon, fichier);

		fclose(fichier);
		free(tampon);

		return true;
	} else {
		if (errno == ENOENT) envoyerReponse(404, "Utilisateur non trouvé");
		else envoyerReponse(500, "Erreur à l'ouverture du compte");
		return false;
	}
}

/**
 * Supprime le partage pointé par idPartage
 * Si le retour est "false" la commande envoie la réponse au client
 * @param username Nom du compte courant
 * @param idPartage Id du partage à supprimer
 * @return True si suppression réussie. Faux sinon.
 */
bool USER_detruirePartage(const char* username, const char* idPartage) {

	FILE* fichier;
	if (ouvrirFichier(".shares", &fichier, "r")) {

		// On cherche à connaitre la taille du fichier pour pouvoir le stocker.
		fseek(fichier, 0, SEEK_END);
		long tailleFichier = ftell(fichier) + 2;
		char* tampon = calloc(tailleFichier, sizeof(char)); // On va y stocker temporairement le fichier
		rewind(fichier); // On repart au début


		char* ligneFichier = NULL;
		size_t len = 0; // Longueur du bloc mémoire "ligneFichier" utilisé uniquement pour getline()
		int numDePartage = 1;
		char* encUsername = encodeURI(username);

		while (getline(&ligneFichier, &len, fichier) != -1) {

			char* separateur = strchr(ligneFichier, ' ');

			if (separateur == NULL) {
				envoyerReponse(500, "Erreur Fichier partages corompu");
				return false;
			}
			else if (strncmp(ligneFichier, encUsername, separateur - ligneFichier) == 0 && ligneFichier[strlen(encUsername)] == ' ') { // Si c'est un partage donné

				if (numDePartage != atoi(idPartage)) strcat(tampon, ligneFichier); //On n'est pas sur la bonne ligne on n'y touche pas
				numDePartage ++;

			} else strcat(tampon, ligneFichier); //On n'est pas sur la bonne ligne on n'y touche pas
		}

		// On ecrit la nouvelle version dans le fichier
		// On vide le fichier et récupère les droits d'écriture
		fichier = freopen(NULL, "w", fichier);
		fputs(tampon, fichier);

		fclose(fichier);
		free(tampon);
		free(ligneFichier);
		free(encUsername);

		return true;
	} else {
		envoyerReponse(500, "Echec à l'ouverture des partages");
		return false;
	}
}

/**
 * Modifications des infos d'un contact
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 23/01/2021
 * @param username de l'utilisateur courant
 * @param idContact id du contact à modifier
 * @param nomDuChamp
 * @param nouvelleValeur Valeur à modifier
 * @return True si modification réussie. Faux sinon.
 */
bool USER_modifierContact(const char* username, const char* idContact, const char* nomDuChamp, const char* nouvelleValeur) {

	FILE* fichier;
	if (ouvrirFichier(username, &fichier, "r")) {

		// On cherche à connaitre la taille du fichier pour pouvoir le stocker.
		fseek(fichier, 0, SEEK_END);
		long tailleFichier = ftell(fichier) + 2;
		char* tampon = calloc(tailleFichier, sizeof(char)); // On va y stocker temporairement le fichier
		rewind(fichier); // On repart au début


		char* ligneFichier = NULL;
		size_t len = 0; // Longueur du bloc mémoire "ligneFichier" utilisé uniquement pour getline()
		int numDeLigne = 1;

		// On saute la première ligne qui contient le mot de passe du compte
		getline(&ligneFichier, &len, fichier);
		strcpy(tampon, ligneFichier); // On conserve le mot de passe

		while (getline(&ligneFichier, &len, fichier) != -1) {

			if (numDeLigne == atoi(idContact)) { // On est arrivé sur la bonne ligne

				int numChamp;
				// La série de if elseif est moche mais on n'a pas vraiment le choix
				if (strcmp(nomDuChamp, "nom") == 0) numChamp = 0;
				else if (strcmp(nomDuChamp, "prenom") == 0) numChamp = 1;
				else if (strcmp(nomDuChamp, "mail") == 0) numChamp = 2;
				else if (strcmp(nomDuChamp, "telephone") == 0) numChamp = 3;
				else if (strcmp(nomDuChamp, "adresse") == 0) numChamp = 4;
				else {
					char* msg = calloc(56 + strlen(nomDuChamp), sizeof(char));
					sprintf(msg, "Erreur de syntaxe: '%s' n'est pas un nom de champ valable", nomDuChamp);
					envoyerReponse(400, msg);
					free(msg);
					return false;
				}

				// On délimite le début et la fin du champ à modifier
				char* separateurDebut = ligneFichier;
				char* separateurFin = strchr(ligneFichier, ' ');
				for (int i = 0; i < numChamp; ++i) {
					separateurDebut = separateurFin + 1;
					separateurFin = strchr(separateurFin+1, ' ');

					if (separateurFin == NULL) { // On est arrivé à la fin de la ligne
						separateurFin = ligneFichier + strlen(ligneFichier) -1;
						break;
					}
				}

				char* encNouvelleValeur = encodeURI(nouvelleValeur);
				// Si la nouvelle valeur du champ est plus grande que la taille du tampon
				// Si ce n'est pas le cas on n'utilise pas toute la taille allouée
				if (separateurFin - separateurDebut < strlen(encNouvelleValeur))
					tampon = realloc(tampon, (tailleFichier - (separateurFin - separateurDebut) + strlen(encNouvelleValeur) +2) * sizeof(char));

				strncat(tampon, ligneFichier, separateurDebut - ligneFichier); // On ajoute la première partie de la ligne
				strcat(tampon, encNouvelleValeur); // La valeur en question
				strcat(tampon, separateurFin); // On ajoute la seconde partie de la ligne
				free(encNouvelleValeur);

			} else strcat(tampon, ligneFichier); //On n'est pas sur la bonne ligne on n'y touche pas

			numDeLigne ++;
		}
		free(ligneFichier);

		// Le fichier contient moins de lignes que le num de contact demandé
		if (numDeLigne - 1 < atoi(idContact)) {
			fclose(fichier);
			free(tampon);
			envoyerReponse(404, "Modification impossible : contact introuvable");
			return false;
		}

		// On ecrit la nouvelle version dans le fichier
		// On vide le fichier et récupère les droits d'écriture
		fichier = freopen(NULL, "w", fichier);
		fputs(tampon, fichier);

		fclose(fichier);
		free(tampon);

		return true;
	} else {
		if (errno == ENOENT) envoyerReponse(404, "Utilisateur non trouvé");
		else envoyerReponse(500, "Erreur à l'ouverture du compte");
		return false;
	}
}

/**
 * Listes les contacts d'un utilisateur.
 * Si le retour est "NULL" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 22/01/2021
 * @param username Nom d'utilisateur de l'annuaire
 * @return La chaine des contacts
 */
char* USER_afficherContacts(const char* username) {

	FILE* fichier;
	if (ouvrirFichier(username, &fichier, "r")) {
		// Vars de lecture dans le fichier
		char* ligneFichier = NULL;
		size_t len = 0; // Longueur du bloc mémoire "ligneFichier" utilisé uniquement pour getline()
		ssize_t lenLigneFichier = 0; // Nombre de caractères lus par getLine()

		// Vars pour l'affichage des contacts
		char* sortie = calloc(1, sizeof(char));
		int compteContacts = 1;

		// On saute la première ligne qui contient le mot de passe du compte
		getline(&ligneFichier, &len, fichier);

		while ((lenLigneFichier = getline(&ligneFichier, &len, fichier)) != -1) {

			char** contact = NULL;
			// On réutilise découpage requête pour ranger notre contact dans un tableau
			int nbrChampsContact = decoupageRequete(ligneFichier, &contact);

			sortie = realloc(sortie, strlen(sortie)
										+ 22 // La partie statique de la chaine du prochain sprintf() fait 20 de long + 1 pour '\0'
										+ (int)log10(compteContacts)
										+ strlen(contact[0])
										+ strlen(contact[1])
										+ strlen(contact[2]) );

			// On affiche la partie minimale d'un contact
			sprintf(sortie + strlen(sortie), "\n \x1B[34m%d ->\x1B[0m %s %s\n\t\t%s\n", compteContacts, contact[0], contact[1], contact[2]);

			// On affiche les composantes facultatives du contact
			for (int i = 3; i < nbrChampsContact; ++i) {
				if (contact[i] != NULL) { // Un champ non renseigné peut être null
					sortie = realloc(sortie, strlen(sortie) + 4 + strlen(contact[i]));
					sprintf(sortie + strlen(sortie), "\t\t%s\n", contact[i]);
				}
			}

			// On nettoie contact
			for (int i = 0; i < nbrChampsContact; ++i) {
				free(contact[i]);
			}
			free(contact);
			compteContacts ++;
		}
		fclose(fichier);
		free(ligneFichier);
		return sortie;
	} else {
		if (errno == ENOENT) envoyerReponse(404, "Utilisateur non trouvé");
		else envoyerReponse(500, "Erreur à l'ouverture du compte");
		return NULL;
	}
}

/**
 * Affiche les contacts d'un annuaire partagé
 * Si le retour est "NULL" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 28/01/2021
 * @param username Utilisateur à l'origine de la requête
 * @param idPartageAAfficher Id du partage de cet utilisateur
 * @return La chaine des contacts
 */
char* USER_afficherContactsPartages(const char* username, int idPartageAAfficher) {

	// Etape 1: Retrouver l'annuaire demandé
	char* compteCible = NULL;

	FILE* fichier;
	if (ouvrirFichier(".shares", &fichier, "r")) {

		char* encUsername = encodeURI(username);
		int nbrRecus = 0;

		char* ligneFichier = NULL;
		size_t len = 0; // Longueur du bloc mémoire "ligneFichier" utilisé uniquement pour getline()
		ssize_t lenLigneFichier = 0; // Nombre de caractères lus par getLine()

		// On lit tout le fichier
		while ((lenLigneFichier = getline(&ligneFichier, &len, fichier)) != -1) {

			ligneFichier[lenLigneFichier - 1] = '\0'; // On retire le '\n' de fin de ligne
			char* separateur = strchr(ligneFichier, ' ');

			if (separateur == NULL) {
				envoyerReponse(500, "Erreur: Fichier partages corompu");
				return NULL;
			}
			// Si c'est un partage reçu && ce n'est pas une partie d'un nom composé
			else if (strcmp(separateur+1, encUsername) == 0) {

				nbrRecus ++;
				if (nbrRecus == idPartageAAfficher) {
					compteCible = calloc(separateur - ligneFichier +2, sizeof(char));
					strncpy(compteCible, ligneFichier, separateur - ligneFichier);
					break;
				}
			}
		}
		fclose(fichier);
		free(ligneFichier);
		free(encUsername);

	} else {
		envoyerReponse(500, "Echec à l'ouverture des partages");
		return NULL;
	}

	// Etape2: Afficher l'annuaire en question
	if (compteCible == NULL) {
		envoyerReponse(404, "Erreur Partage non trouvé");
		return NULL;
	}
	else {
		char* annuaire = USER_afficherContacts(compteCible);
		free(compteCible);
		return annuaire;
	}
}

/**
 * Listes les partages d'un utilisateur.
 * Si le retour est "NULL" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 20/01/2021
 * @param username Nom d'utilisateur du compte
 * @return La chaine des partages
 */
char* USER_afficherPartages(const char* username) {

	FILE* fichier;
	if (ouvrirFichier(".shares", &fichier, "r")) {

		char* encUsername = encodeURI(username);

		char* partDonnes = calloc(1, sizeof(char)); // Stockage temporaire des partages donnés et reçus
		int nbrDonnes = 1;
		char* partRecus = calloc(1, sizeof(char));
		int nbrRecus = 1;

		char* ligneFichier = NULL;
		size_t len = 0; // Longueur du bloc mémoire "ligneFichier" utilisé uniquement pour getline()
		ssize_t lenLigneFichier = 0; // Nombre de caractères lus par getLine()

		// On lit tout le fichier
		while ((lenLigneFichier = getline(&ligneFichier, &len, fichier)) != -1) {

			ligneFichier[lenLigneFichier - 1] = '\0'; // On retire le '\n' de fin de ligne
			char* separateur = strchr(ligneFichier, ' ');

			if (separateur == NULL) {
				envoyerReponse(500, "Erreur: Fichier partages corompu");
				return NULL;
			}
			// Si c'est un partage donné && ce n'est pas une partie d'un nom composé
			else if (strncmp(ligneFichier, encUsername, separateur - ligneFichier) == 0 && ligneFichier[separateur - ligneFichier] == ' ') {

				// Le 14 correspond à la longueur de la chaine ajoutée deux lignes plus bas
				partDonnes = realloc(partDonnes, strlen(partDonnes) + 16 + (int)log10(nbrDonnes) + (separateur - ligneFichier));
				strcat(partDonnes, "\n \x1B[35m"); // Cette chaine fait 7char de long
				sprintf(partDonnes + strlen(partDonnes), "%d", nbrDonnes);
				strcat(partDonnes, " -\x1B[0m "); // Cette chaine fait 7char de long
				strcat(partDonnes, separateur + 1); // On ajoute 1 pour ne pas garder l'espace de séparation
				nbrDonnes ++;

			// Si c'est un partage reçu && ce n'est pas une partie d'un nom composé
			} else if (strcmp(separateur+1, encUsername) == 0) {

				// Le 16 correspond à la longueur de la chaine ajoutée deux lignes plus bas
				partRecus = realloc(partRecus, strlen(partRecus) + 16 + (int)log10(nbrDonnes) + strlen((separateur + 1)));
				strcat(partRecus, "\n \x1B[34m"); // Cette chaine fait 7char de long
				sprintf(partRecus + strlen(partRecus), "%d", nbrRecus);
				strcat(partRecus, " ->\x1B[0m "); // Cette chaine fait 8char de long
				strncat(partRecus, ligneFichier, separateur - ligneFichier);
				nbrRecus ++;
			}
		}
		fclose(fichier);
		free(ligneFichier);
		free(encUsername);

		// Taille des chaines + 116 pour les "titres" cf. plus bas
		char* sortie = calloc(strlen(partDonnes) + strlen(partRecus) + 116, sizeof(char));
		strcpy(sortie, "\x1B[35mListe des utilisateurs pouvant accéder à votre annuaire:\x1B[0m");
		strcat(sortie, partDonnes);
		strcat(sortie, "\n\n\x1B[34mListe des annuaires accessibles:\x1B[0m");
		strcat(sortie, partRecus);

		free(partDonnes);
		free(partRecus);
		return sortie;

	} else {
		envoyerReponse(500, "Echec à l'ouverture des partages");
		return NULL;
	}
}

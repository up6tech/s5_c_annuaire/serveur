//
// Created by Dorian Gardes on 13/01/2021.
//

#ifndef SERVEUR_REQUSERS_H
#define SERVEUR_REQUSERS_H

/**************************************************
 * 												  *
 *     TRAITEMENT DES REQUETES UTILISATEURS       *
 * 												  *
 **************************************************/

/**
 * Création d'un contact dans un annuaire
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 14/01/2021
 * @param username Nom d'utilisateur du compte
 * @param tabArgs Tableau regroupant les différents champs d'un contact. Les champs factultatifs peuvent prendre la valeur NULL.
 * @return True si compté créé avec succès. Faux sinon.
 */
bool USER_creerContact(const char* username, const char** tabArgs);

/**
 * Création d'un partage d'annuaire
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 19/01/2021
 * @param username Nom d'utilisateur du compte
 * @param idBeneficiaire Id du bénéficiaire du partage
 * @return True si partage créé avec succès. Faux sinon.
 */
bool USER_creerPartage(const char* username, int beneficiaire);

/**
 * Supprime le contact pointé par idContact
 * Si le retour est "false" la commande envoie la réponse au client
 * @param username Nom du compte courant
 * @param idContact Id du contact à supprimer
 * @return True si suppression réussie. Faux sinon.
 */
bool USER_detruireContact(const char* username, const char* idContact);

/**
 * Supprime le contact pointé par idContact
 * Si le retour est "false" la commande envoie la réponse au client
 * @param username Nom du compte courant
 * @param idPartage Id du partage à supprimer
 * @return True si suppression réussie. Faux sinon.
 */
bool USER_detruirePartage(const char* username, const char* idPartage);

/**
 * Modifications des infos d'un contact
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 23/01/2021
 * @param username de l'utilisateur courant
 * @param idContact id du contact à modifier
 * @param nomDuChamp
 * @param nouvelleValeur Valeur à modifier
 * @return True si modification réussie. Faux sinon.
 */
bool USER_modifierContact(const char* username, const char* idContact, const char* nomDuChamp, const char* nouvelleValeur);

/**
 * Listes les contacts d'un utilisateur.
 * Si le retour est "NULL" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 22/01/2021
 * @param username Nom d'utilisateur de l'annuaire
 * @return La chaine des contacts
 */
char* USER_afficherContacts(const char* username);

/**
 * Affiche les contacts d'un annuaire partagé
 * Si le retour est "NULL" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 28/01/2021
 * @param username Utilisateur à l'origine de la requête
 * @param idPartageAAfficher Id du partage de cet utilisateur
 * @return La chaine des contacts
 */
char* USER_afficherContactsPartages(const char* username, int idPartageAAfficher);

/**
 * Listes les partages d'un utilisateur.
 * Si le retour est "NULL" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 20/01/2021
 * @param username Nom d'utilisateur du compte
 * @return La chaine des partages
 */
char* USER_afficherPartages(const char* username);

#endif //SERVEUR_REQUSERS_H

//
// Created by Dorian Gardes on 02/01/2021.
//

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>
#include <dirent.h>
#include <math.h>
#include <stdlib.h>

#include "aiguillageServeur.h"
#include "Entres Sorties/socketServeur.h"
#include "Requetes/reqAdmin.h"
#include "Requetes/reqUsers.h"
#include "utils.h"

/**************************************************
 * 												  *
 *       CENTRALISATION DES ENTREES/SORTIES       *
 * 												  *
 **************************************************/


/**************************************
 * GESTION ET TRAITEMENT DES REQUETES
 **************************************/

/**
 *
 * @author Dorian Gardes
 * @date 02/01/2021
 * @param codeReponse
 * @param message Charge utile de la réponse.
 * @return 1 si l'envoi s'est bien passé, 0 sinon.
 */
int envoyerReponse(int codeReponse, char* message) {

	char* encodedMessage = encodeURI(message);

	   // Taille:
	  //	12 pour l'en-tête et les espaces entre le message, codeErreur et en-tête. On ajoute deux char de marge au cas où message soit vide
	 //		(int)log10(codeReponse) Taille en nbr de char de codeReponse
	//		strlen(message) Il faut l'expliquer ?
	char* reponse = calloc(12 + (int)log10(codeReponse) + strlen(encodedMessage) , sizeof(char));

	sprintf(reponse, "AUPSCG %d ", codeReponse);
	if (message == NULL || strcmp(message, "") == 0) strcat(reponse, "OK");
	else {
		strcat(reponse, encodedMessage);
	}
	if (reponse[strlen(reponse) - 1] != '\n') strcat(reponse, "\n");

	#ifdef DEBUG
		printf("OUT: %s\n", reponse);
	#endif
	int retour = Emission(reponse);
	free(reponse);
	free(encodedMessage);
	return retour;
}

/**************************************
 * ADMINISTRATION DES COMPTES
 **************************************/

/**
 * Traitement de la requête créer
 * Cette fonction sert principalement d'aiguillage entre le traitement admin et utilisateur standard
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 13/01/2021
 * @param tabRequete Pointeur vers un tableau généré par decoupageRequete()
 * @param lenTabRequete Nbr de cases de tabRequete
 * @return True si compté créé avec succès. Faux sinon.
 */
bool creer(char** tabRequete, int lenTabRequete) {

	if (isAdmin(tabRequete[USERNAME])) {
		return ADMIN_creerCompte(tabRequete[ARGS], tabRequete[ARGS + 1]);
	} else if (strcmp(tabRequete[ARGS], "contact") == 0) {

		if (lenTabRequete < 8 || lenTabRequete > 10) {
			envoyerReponse(400, "Nombre d'arguments incorrects");
			return false;
		}

		char* tabArgs[] = {NULL, NULL, NULL, NULL, NULL};
		// On retire 5 à lenTabRequete pour les 4 champs d'en-tête + le champ de choix partage/contact (voir deux lignes plus haut)
		for (int i = 0; i < lenTabRequete - 5; ++i) {
			tabArgs[i] = encodeURI(tabRequete[ARGS +1 + i]); // On copie la référence des args
		}
		// On crée le contact
		bool retour = USER_creerContact(tabRequete[USERNAME], (const char **) tabArgs);
		// On nettoie les miettes
		for (int i = 0; i < lenTabRequete - 5; ++i) {
			free(tabArgs[i]);
		}
		return retour;
	} else if (strcmp(tabRequete[ARGS], "partage") == 0) {

		if (lenTabRequete != 6) {
			envoyerReponse(400, "Nombre d'arguments incorrects");
			return false;
		}
		return USER_creerPartage(tabRequete[USERNAME], atoi(tabRequete[ARGS +1]));
	} else {
		char* msg = calloc(strlen(tabRequete[ARGS]) + 42, sizeof(char));
		sprintf(msg, "Mauvaise requête: Commande %s introuvable\n", tabRequete[ARGS]);
		envoyerReponse(400, msg);
		free(msg);
		return false;
	}

}

/**
 * Traitement de la requête détruire
 * Cette fonction sert d'aiguillage entre le traitement admin et utilisateur standard
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 13/01/2021
 * @param tabRequete Tableau symbolisant la requête.
 * @return True si compte détruit avec succès. Faux sinon.
 */
bool detruire(char** tabRequete) {

	if (isAdmin(tabRequete[USERNAME])) {
		return ADMIN_detruireCompte(tabRequete[ARGS]);
	} else if (strcmp(tabRequete[ARGS], "contact") == 0) {
		return USER_detruireContact(tabRequete[USERNAME], tabRequete[ARGS + 1]);
	} else if (strcmp(tabRequete[ARGS], "partage") == 0) {
		return USER_detruirePartage(tabRequete[USERNAME], tabRequete[ARGS + 1]);
	} else {
		char* msg = calloc(strlen(tabRequete[ARGS]) + 42, sizeof(char));
		sprintf(msg, "Mauvaise requête: Commande %s introuvable\n", tabRequete[ARGS]);
		envoyerReponse(400, msg);
		free(msg);
		return false;
	}
}

/**
 * Traitement de la requête modifier
 * Cette fonction sert d'aiguillage entre le traitement admin et utilisateur standard
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 13/01/2021
 * @param username de l'utilisateur courant
 * @param idCompte id du compte/contact à modifier
 * @param nomDuChamp
 * @param nouvelleValeur Valeur à modifier
 * @return True si modification réussie. Faux sinon.
 */
bool modifier(const char* username, const char* idCompte, const char* nomDuChamp, const char* nouvelleValeur) {

	if (isAdmin(username)) return ADMIN_modifierCompte(idCompte, nomDuChamp, nouvelleValeur);
	else return USER_modifierContact(username, idCompte, nomDuChamp, nouvelleValeur);
}

/**
 * Traitement de la requête afficher
 * Cette fonction sert principalement d'aiguillage entre le traitement admin et utilisateur standard
 * Si le retour est "NULL" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 13/01/2021
 * @param tabRequete Pointeur vers un tableau généré par decoupageRequete()
 * @param lenTabRequete Nbr de cases de tabRequete
 * @return Une chaine correspondant à la réponse de la requête.
 */
char* afficher(char** tabRequete, int lenTabRequete) {

	if (isAdmin(tabRequete[USERNAME]) || strcmp(tabRequete[ARGS], "comptes") == 0) {
		return ADMIN_afficherComptes();
	} else if (strcmp(tabRequete[ARGS], "annuaire") == 0) {
		if (lenTabRequete == 5) return USER_afficherContacts(tabRequete[USERNAME]);
		else return USER_afficherContactsPartages(tabRequete[USERNAME], atoi(tabRequete[ARGS +1]));
	} else if (strcmp(tabRequete[ARGS], "partages") == 0) {
		return USER_afficherPartages(tabRequete[USERNAME]);
	} else {
		char* msg = calloc(strlen(tabRequete[ARGS]) + 42, sizeof(char));
		sprintf(msg, "Mauvaise requête: Commande %s introuvable\n", tabRequete[ARGS]);
		envoyerReponse(400, msg);
		free(msg);
		return false;
	}
}

/**
 * Change le mot de passe du compte courant
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 04/01/2021
 * @param username de l'utilisateur courant
 * @param nouveauMotDePasse Mot de passe (en clair) à donner au compte admin.
 * @return True si changement réussi. False sinon.
 */
bool editMotDePasse(const char* username, const char* nouveauMotDePasse) {

	const char* nomFichier;
	if (isAdmin(username)) nomFichier = ".adm";
	else nomFichier = username;
	FILE* fichier = NULL;
	if (ouvrirFichier(nomFichier, &fichier, "r")) {

		// Etape 1: Connaitre la taille du fichier pour pouvoir le stocker.
		// Comme le mot de passe est hashé en SHA512 il a une taille fixe.
		fseek(fichier, 0, SEEK_END);
		char* tampon = calloc(ftell(fichier) + 2, sizeof(char)); // On va y stocker temporairement le fichier
		rewind(fichier); // On repart au début

		// Etape 2: On remplace le mot de passe
		char* ligne = NULL;
		size_t len = 0;
		getline(&ligne, &len, fichier); // Lecture de l'ancien mot de passe
		char* encodedPassword = sha512(nouveauMotDePasse);
		strcpy(tampon, encodedPassword);
		strcat(tampon, "\n");
		free(encodedPassword);

		while (getline(&ligne, &len, fichier) != -1) {
			strcat(tampon, ligne);
		}

		// Etape 3: On ecrit dans le fichier sans les partages du compte a supprimer
		// On vide le fichier et récupère les droits d'écriture
		fichier = freopen(NULL, "w", fichier);
		fputs(tampon, fichier);

		fclose(fichier);
		free(tampon);
		return true;

	} else {
		if (errno == ENOENT) envoyerReponse(404, "Utilisateur non trouvé");
		else envoyerReponse(500, "Erreur à l'ouverture du compte");
		return false;
	}
}

/**
 * Retourne le nom du compte représenté par le
 * @author Dorian Gardes
 * @date 04/01/2021
 * @param numero Numéro de compte donné par afficherComptes()
 * @return Le nom du compte correspondant. Si le numéro est introuvable renvoie NULL;
 */
char* getNomCompteFromNumero(int numero) {

	DIR* dossier = opendir(CHEMIN_BASE);
	if (dossier != NULL) {

		char* nomCompte = NULL;
		int i = 1;
		struct dirent* fichier;
		while ((fichier = readdir(dossier)) != NULL) {

			// Si c'est un dossier ou un fichier caché on saute
            #ifdef WIN32
            if ((strchr(fichier->d_name, '.')) == NULL || (fichier->d_name)[0] == '.') continue;
            #else
            if (fichier->d_type == 4 || (fichier->d_name)[0] == '.') continue;
            #endif

			if (i == numero) {
				nomCompte = calloc(strlen(fichier->d_name) + 2, sizeof(char));
				strcpy(nomCompte, fichier->d_name);
				break;
			}
			i++;
		}
		closedir(dossier);

		if (nomCompte == NULL) envoyerReponse(404, "Impossible de retrouver le compte.");
		return nomCompte;

	} else {
		envoyerReponse(500, "Impossible d'ouvrir la base des comptes");
		return NULL; // Impossible d'ouvrir le dossier des données
	}
}

/**************************************
 * GESTION DU SYSTEME DE FICHIER
 **************************************/

/**
 * Tente d'ouvrir un fichier et affiche le message d'erreur si nécéssaire
 * @author Dorian Gardes
 * @date 03/01/2021
 * @param nom Nom du fichier à ouvrir
 * @param fichier Pointeur du fichier ouvert
 * @param mode Mode d'ouverture du fichier. Voir fopen()
 * @return True si ouvert avec succès. Faux sinon.
 */
bool ouvrirFichier(const char* nom, FILE** fichier, const char* mode) {

	char* nomFichier = getCheminFichier(nom);

	*fichier = fopen(nomFichier, mode);
	if (*fichier == NULL) {
		char* base = "Impossible d'ouvrir le fichier";
		switch (errno) {
			case ENOENT:
				fprintf(stderr, "%s %s : Fichier inexistant\n", base, nom);
				break;
			case EACCES:
				fprintf(stderr, "%s %s : Permissions insuffisantes\n", base, nom);
				break;
			case EINVAL:
				fprintf(stderr, "%s %s : Nom de fichier invalide (caractères non supportés par le système de fichier)\n", base, nom);
				break;
			case ENAMETOOLONG:
				fprintf(stderr, "%s %s : Chemin vers le fichier trop long\n", base, nom);
				break;
			case EROFS:
				fprintf(stderr, "%s %s : Système de fichier en lecture seule\n", base, nom);
				break;
			default:
				fprintf(stderr, "%s %s : Raison inconnue\n", base, nom);
				break;
		}
		free(nomFichier);
		return false;
	}
	free(nomFichier);
	return true;
}

/**
 * Donne le chemin relatif pour un nom de fichier donné
 * @author Dorian Gardes
 * @date 03/01/2021
 * @param nom Nom du fichier
 * @return chemin vers ce fichier
 */
char* getCheminFichier(const char* nom) {

	char* nomFichier = calloc(strlen(nom) + strlen(CHEMIN_BASE) + 2, sizeof(char));
	strcpy(nomFichier, CHEMIN_BASE);
	strcat(nomFichier, nom);
	return nomFichier;
}

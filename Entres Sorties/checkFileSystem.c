//
// Created by Dorian Gardes on 03/01/2021.
//

#include <stdio.h>
#include <stdbool.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <malloc.h>

#include "checkFileSystem.h"
#include "../aiguillageServeur.h"
#include "../utils.h"

/**************************************************
 *     VERIFICATION INITIALE DE L'ARBORESSENCE    *
 *         Séparé du reste car assez long         *
 **************************************************/

/**
 * Méthode privée pour créer le dossier des données
 * @return True si réussi
 */
static bool tryCreateFolder() {

    #ifdef WIN32
    int result = mkdir(CHEMIN_BASE);
    #else
    int result = mkdir(CHEMIN_BASE, 0700);
    #endif
	if (result == 0) {
		printf("Création du dossier de données réussie.\n");
		fflush(stdin);
		return true;
	}
	else {
		char* base = "Impossible de créer le dossier";
		switch (errno) {
			case EACCES:
				fprintf(stderr, "%s %s : Permissions insuffisantes\n", base, CHEMIN_BASE);
				break;
			case EINVAL:
				fprintf(stderr, "%s %s : Nom de dossier invalide (caractères non supportés par le système de fichier)\n", base, CHEMIN_BASE);
				break;
			case ENAMETOOLONG:
				fprintf(stderr, "%s %s : Chemin vers le dossier trop long\n", base, CHEMIN_BASE);
				break;
			case EROFS:
				fprintf(stderr, "%s %s : Système de fichier en lecture seule\n", base, CHEMIN_BASE);
				break;
			case ENOTDIR:
				fprintf(stderr, "%s %s : Nom de dossier invalide (c'est un nom de fichier)\n", base, CHEMIN_BASE);
				break;
			default:
				fprintf(stderr, "%s %s : Raison inconnue\n", base, CHEMIN_BASE);
				break;
		}
		return false;
	}
}

/**
 * Méthode privée pour créer le fichier .adm (mot de passe admin)
 * @return True si réussi
 */
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpointer-sign"
static bool tryCreateAdm() {

	FILE* fichier = NULL;
	if (ouvrirFichier(".adm", &fichier, "w")) {
		char* hash = sha512("admin");
		fprintf(fichier, "%s\n", hash);
		free(hash);
		fclose(fichier);
		return true;
	} else return false;
}
#pragma clang diagnostic pop

/**
 * Méthode privée pour créer le fichier .shares (définitions des partages)
 * @return True si réussi
 */
static bool tryCreateShares() {

	FILE* fichier = NULL;
	if (ouvrirFichier(".shares", &fichier, "w")) {
		fclose(fichier);
		return true;
	} else return false;
}

/**
 * Vérifie que les fichiers nécessaires au fonctionnement du programme sont présents.
 * Crée les fichiers manquants avec les valeurs par défaut si besoin.
 * @author Dorian Gardes
 * @date 03/01/2020
 * @return true si les fichiers sont présents et/ou recréés.
 */
bool checkArborescence() {

	// Premier check : Le dossier existe ?
	DIR* dossier = opendir(CHEMIN_BASE);
	if (dossier == NULL) { // Le dossier n'a pas pu être ouvert
		switch (errno) {
			case EACCES:
				fprintf(stderr, "Impossible d'accéder au dossier %s : Permissions insuffisantes\n", CHEMIN_BASE);
				return false;
			case ENOENT:
				fprintf(stderr, "Dossier des données introuvables. Tentative de création.\n");
				if (!tryCreateFolder()) return false;
				break;
			default:
				fprintf(stderr, "Impossible d'accéder au dossier %s : Raison inconnue\n", CHEMIN_BASE);
				return false;
		}
	}
	closedir(dossier);
	// Premier check terminé

	FILE* fichier = NULL;

	// Second check : Fichier de compte admin
	if (!ouvrirFichier(".adm", &fichier, "r")) { // Le fichier n'existe pas
		if (!tryCreateAdm()) return false;
		printf("Fichier .adm créé\n");
	}
	// Second check terminé

	// Troisième check : Fichier pour les partages
	if (!ouvrirFichier(".shares", &fichier, "r")) { // Le fichier n'existe pas
		if (!tryCreateShares()) return false;
		printf("Fichier .shares créé\n");
	}
	// Troisième check terminé

	return true;
}
#ifndef __SOCKET_SERVEUR_H__
#define __SOCKET_SERVEUR_H__

#include <stdbool.h>

/**************************************************
 * 												  *
 *               GESTION DES SOCKETS              *
 *               DONNE AVEC LE SUJET              *
 * 												  *
 **************************************************/

#define LONGUEUR_TAMPON 4096

/* Initialisation.
 * Creation du serveur.
 * renvoie 1 si ça c'est bien passé 0 sinon
 */
int Initialisation();

/* Initialisation.
 * Creation du serveur en précisant le service ou numéro de port.
 * renvoie 1 si ça c'est bien passé 0 sinon
 */
int InitialisationAvecService(char *service);

/**
 * Permet de savoir si l'on attend une requête client.
 * @author Dorian Gardes
 * @date 28/01/2021
 * @return Vrai si on attend une requête client. Faux sinon.
 */
bool isWaitingForClient();

/* Attends qu'un client se connecte.
 * renvoie 1 si ça c'est bien passé 0 sinon
 */
int AttenteClient();

/* Recoit un message envoye par le client.
 * retourne le message ou NULL en cas d'erreur.
 * Note : il faut liberer la memoire apres traitement.
 */
char *Reception();

/* Envoie un message au client.
 * Attention, le message doit etre termine par \n
 * renvoie 1 si ça c'est bien passé 0 sinon
 */
int Emission(char *message);

/* Ferme la connexion avec le client.
 */
void TerminaisonClient();

/* Arrete le serveur.
 */
void Terminaison();

#endif

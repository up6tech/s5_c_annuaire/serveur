//
// Created by Dorian Gardes on 03/01/2021.
//

#ifndef SERVEUR_CHECKFILESYSTEM_H
#define SERVEUR_CHECKFILESYSTEM_H

#include <stdbool.h>

/**************************************************
 *     VERIFICATION INITIALE DE L'ARBORESSENCE    *
 *         Séparé du reste car assez long         *
 **************************************************/

/**
 * Vérifie que les fichiers nécessaires au fonctionnement du programme sont présents.
 * Crée les fichiers manquants avec les valeurs par défaut si besoin.
 * @author Dorian Gardes
 * @date 03/01/2020
 * @return true si les fichiers sont présents et/ou recréés.
 */
bool checkArborescence();

#endif //SERVEUR_CHECKFILESYSTEM_H

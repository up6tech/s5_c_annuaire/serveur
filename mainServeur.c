#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <signal.h>
#include <errno.h>

#include "Entres Sorties/socketServeur.h"
#include "Entres Sorties/checkFileSystem.h"
#include "aiguillageServeur.h"
#include "utils.h"

/**************************************************
 * 												  *
 *               SERVEUR D'ANNUAIRE               *
 *                UPSSITECH STRI 1A               *
 *                  Janvier 2021                  *
 *          CAYRE Fabien & GARDES Dorian          *
 * 												  *
 **************************************************/

bool stop = false;

/**
 * Fonction pour intercepter les signaux et fermer le serveur proprement.
 * @author Dorian Gardes
 * @date 08/01/2021
 * @param signal Numéro de signal
 */
void signalCatcher(int signal) {

	if (isWaitingForClient()) {
		TerminaisonClient();
		Terminaison();
		printf("\rFermeture du serveur réussie\n");
		exit(EXIT_SUCCESS);
	} else stop = true;
}

/**
 * FONCTION MAIN
 */
int main() {

	// On importe l'encodage courant pour pouvoir gérer les accents correctement
	setlocale(LC_ALL, "");
	stop = false;

	// On intercepte le Ctrl+C (SIGINT)
	struct sigaction act;
	act.sa_handler = signalCatcher;
	sigaction(SIGINT, &act, NULL);

    // Si l'arborescence est endommagée et que la réparation a échouée on stoppe.
    // On n'affiche pas de message d'erreur la fonction gère tout.
	if (!checkArborescence()) return EXIT_FAILURE;

    if (!Initialisation()) {
		printf("Impossible d'ouvrir le socket.\n");
		return EXIT_FAILURE;
    }
    printf("Quitter avec Ctrl+C\n");


    #pragma ide diagnostic ignored "EndlessLoop"
    while(!stop) {

        AttenteClient();

        char* requete = Reception();
		if (requete == NULL) continue; // Si le client courant plante on saute une requete = NULL
		#ifdef DEBUG
        	printf("IN: %s", requete);
		#endif

		if (strncmp("htcpcp", requete, 6) == 0 || strncmp("HTCPCP", requete, 6) == 0) {
			free(requete);
			envoyerReponse(418, "I'm a teapot");
			TerminaisonClient();
			continue;
		}

        // Découpage de la requête
		char** tabReq = NULL;
		int lenTabReq = decoupageRequete(requete, &tabReq);

		free(requete);

        // Etape 1.2: Vérification de la syntaxe
		if (lenTabReq >= 4 && strcmp(tabReq[HEADER], "AUPSCG") == 0) {

				FILE* fichierCompte = NULL;
				char* nomFichierPassword;
				if (isAdmin(tabReq[USERNAME])) nomFichierPassword = ".adm";
				else nomFichierPassword = tabReq[USERNAME];

				if (ouvrirFichier(nomFichierPassword, &fichierCompte, "r")) {
					char* hash = sha512(tabReq[PASSWORD]);
					char storedPassword[130]; // Un hash de mot de passe fait 128 char de long
					// On ne lit que 129 pour ommettre le '\n' de fin de ligne.
					fgets(storedPassword, 129, fichierCompte);

					if (strcmp(hash, storedPassword) != 0) envoyerReponse(403, "Mot de passe incorect");
					else {
						// Authentifié
						TypeRequete typeReq = getTypeRequete(tabReq[TYPEREQ]);
						if (!checkNbrArguments(tabReq, lenTabReq)) envoyerReponse(400, "Nombre d'arguments incorrect");
						else {
							char* chaine = NULL; // On peut pas créer de variable dans un switch donc c'est içi si besoin
							switch (typeReq) {
								case CREER:
									// Si le retour est "false" la commande envoie la réponse
									if (creer(tabReq, lenTabReq)) envoyerReponse(200, "OK");
									break;
								case DETRUIRE:
									// Si le retour est "false" la commande envoie la réponse
									if (detruire(tabReq)) envoyerReponse(200, "OK");
									break;
								case MODIFIER:
									// Si le retour est "false" la commande envoie la réponse
									if (modifier(tabReq[USERNAME], tabReq[ARGS], tabReq[ARGS +1], tabReq[ARGS + 2])) envoyerReponse(200, "OK");
									break;
								case AFFICHER:
									chaine = afficher(tabReq, lenTabReq);
									// Si le retour est "NULL" la commande envoie la réponse
									if (chaine != NULL) {
										envoyerReponse(200, chaine);
										free(chaine);
									}

									break;
								case MOTDEPASSE:
									// Si le retour est "false" la commande envoie la réponse
									if (editMotDePasse(tabReq[USERNAME], tabReq[ARGS])) envoyerReponse(200, "OK");
									break;
								case INCONNU:
								default:
									envoyerReponse(400, "Commande inconnue");
									break;
							} // Switch
						} // Nbr Arguments
					} // Authentification

					free(hash);
					fclose(fichierCompte);
				} else {
					if (errno == ENOENT) envoyerReponse(404, "Utilisateur non trouvé");
					else envoyerReponse(500, "Erreur à l'ouverture du compte");
				}

		} else envoyerReponse(400, "Erreur de syntaxe");

        // On nettoie tout le bordel
		for (int i = 0; i < lenTabReq; ++i) {
			free(tabReq[i]);
		}
		free(tabReq);

        TerminaisonClient();

    } // Fin while(1)
	printf("\rFermeture du serveur réussie\n");
}

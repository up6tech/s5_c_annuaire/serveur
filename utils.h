//
// Created by Dorian Gardes on 05/01/2021.
//

#ifndef SERVEUR_UTILS_H
#define SERVEUR_UTILS_H

// Permet un serveur plus verbeux (il affiche tout ce qui entre et sors)
#define DEBUG

#include <stdbool.h>
#include <stddef.h>

/**************************************************
 * 												  *
 *               FONCTIONS DIVERSES               *
 *    SIMPLIFIE CERTAINES OPERATIONS COMPLEXES    *
 * 												  *
 **************************************************/

/**
 * Indices du tableau renvoyé par decoupageRequete où retrouver les différents champs
 */
#define HEADER 0
#define TYPEREQ 1
#define USERNAME 2
#define PASSWORD 3
#define ARGS 4

typedef enum typeRequete {
	CREER = 0,
	DETRUIRE = 1,
	MODIFIER = 2,
	AFFICHER = 3,
	MOTDEPASSE = 4,
	INCONNU = 5
} TypeRequete;


/**
 * Table statique pour l'utilisation de encodeURI()
 */
static const wchar_t rfc3986[] = {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '-', '.', '\0', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '\0', '\0', '\0', '\0', '\0', '\0', '\0', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '\0', '\0', '\0', '\0', '_', '\0', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '\0', '\0', '\0', '~', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'};

/**
 * Encode une chaine de caractère selon l'encodage pourcent
 * Lâchement volé sur https://rosettacode.org/wiki/URL_encoding#C
 * Largement remanié pour gérer les accents et différentes locales
 * @author Dorian Gardes
 * @date 02/01/2021
 * @param from string décodée
 * @return string encodée
 */
char* encodeURI(const char *from);

/**
 * Décode une chaine de caractère selon l'encodage pourcent
 * Lâchement volé sur https://rosettacode.org/wiki/URL_decoding#C
 * Largement remanié pour gérer les accents et différentes locales
 * @author Dorian Gardes
 * @date 02/01/2021
 * @param from string encodée
 * @return string decodée
 */
char* decodeURI(const char *from);

/**
 * Renvoie le type de requête de façon plus propre
 * (Oui c'est du cache merde j'assume)
 * @author Dorian Gardes
 * @date 04/01/2021
 * @param type La chaine à analyser
 * @return Le type si reconnu. INCONNU sinon.
 */
TypeRequete getTypeRequete(const char* type);

/**
 *
 * @author Dorian Gardes
 * @date 04/01/2021
 * @param req Chaine contenant la requête
 * @param tableau Pointeur vers le tableau défini et rempli par la focntion
 * @return Le nombre d'éléments dans tableau
 */
int decoupageRequete(const char* req, char*** tableau);

/**
 * Vérifie que le nombres d'arguments est cohérent par rapport au type de requete
 * Attention ne vérifie pas le contenu des arguments. En particulier pour les requêtes de type Creer et Afficher qui ont des tailles variables.
 * @author Dorian Gardes
 * @date 14/01/2021
 * @param tabRequete Tableau donné par la f(x) découpageRequete()
 * @param lenTabRequete Nombre de cases de tabRequete
 * @return True si le nombre de cases est cohérent. Faux sinon.
 */
bool checkNbrArguments(char** tabRequete, int lenTabRequete);

/**
 * Détermine si le compte est administrateur
 * @author Dorian Gardes
 * @date 02/01/2021
 * @param username
 * @return True si c'est l'administrateur
 */
bool isAdmin(const char* username);

/**
 * Calcule le hash de la chaine.
 * @author Dorian Gardes
 * @date 03/01/2021
 * @param string Chaine à hasher
 * @return Hash corespondant à string
 */
char* sha512(const char* string);

#endif //SERVEUR_UTILS_H

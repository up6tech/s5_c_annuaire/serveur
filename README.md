# Serveur d'annuaire

Serveur d'annuaire implémentant le protocole AUPSCG dans le cadre du semestre 5 de la formation STRI.

Par défaut le compte administrateur se connecte avec :
    Login : admin
    Mot de passe : admin

# Comment compiler

## Via un IDE (tel qu'Eclipse)

Exécuter la commande suivante pour créer le fichier .projet nécessaire au fonctionnement d'Eclipse (compilation, debug, etc)<br>
``cmake -G "Eclipse CDT4 - Unix Makefiles" ./``

Pour d'autre IDE, veuillez remplacer `"Eclipse CDT4 - Unix Makefiles"` par la liste affichée via:<br>
``cmake -G``

Importer le projet via l'outil de l'IDE et choisir "Projet CMake"

## Via l'outil make des systèmes UNIX

Exécuter la commande suivante pour créer les fichiers make<br>
``cmake -G"Unix Makefiles" ./``

## Bibliothèque(s) requise(s)

Le projet, pour l'instant, ne requiert qu'une seule bibliothèque: 

Installation sous sytème UNIX : ``sudo apt install libssl-dev``

//
// Created by Dorian Gardes on 05/01/2021.
//

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <wchar.h>
#include <malloc.h>
#include <openssl/sha.h>

#include "utils.h"

/**************************************************
 * 												  *
 *               FONCTIONS DIVERSES               *
 *    SIMPLIFIE CERTAINES OPERATIONS COMPLEXES    *
 * 												  *
 **************************************************/

/**
 * Encode une chaine de caractère selon l'encodage pourcent
 * Lâchement volé sur https://rosettacode.org/wiki/URL_encoding#C
 * Largement remanié pour gérer les accents et différentes locales
 * @author Dorian Gardes
 * @date 02/01/2021
 * @param from string décodée
 * @return string encodée
 */
char* encodeURI(const char *from) {

	if (from == NULL) return NULL;
	// On convertit la chaine en chaine de wide char
	wchar_t* wideFrom = calloc(strlen(from) +3, sizeof(wchar_t));
	mbstowcs(wideFrom, from, strlen(from));
	wchar_t* fromBackup = wideFrom; // On le conserve pour le nettoyage

	wchar_t* wideTo = calloc((wcslen(wideFrom) * 3) + 1, sizeof(wchar_t));
	wchar_t* toBackup = wideTo; // On backup puisque to est utilisé dans le while
	for (; *wideFrom; wideFrom++) {
		if (rfc3986[*wideFrom]) swprintf(wideTo, sizeof(wchar_t), L"%lc", rfc3986[*wideFrom]);
		else swprintf(wideTo, 3 * sizeof(wchar_t), L"%%%02X", *wideFrom);
		while (*++wideTo);
	}

	// On alloue la taille max que prendra la chaine
	char* to = calloc(wcslen(toBackup) * 4, sizeof(char));
	// On convertit les wide char en char
	wcstombs(to, toBackup, wcslen(toBackup) * 4);
	free(fromBackup);
	free(toBackup);

	// On redimensionne à la taille réelle
	to = realloc(to, (strlen(to) +1) * sizeof(char));
	return to;
}

static int ishex(int x) {
	return	(x >= '0' && x <= '9')	|| (x >= 'a' && x <= 'f') || (x >= 'A' && x <= 'F');
}

/**
 * Décode une chaine de caractère selon l'encodage pourcent
 * Lâchement volé sur https://rosettacode.org/wiki/URL_decoding#C
 * Largement remanié pour gérer les accents et différentes locales
 * @author Dorian Gardes
 * @date 02/01/2021
 * @param from string encodée
 * @return string decodée
 */
char* decodeURI(const char *from) {

	if (from == NULL) return NULL;
	// On convertit la chaine en chaine de wide char
	wchar_t* wideFrom = calloc(strlen(from) + 2, sizeof(wchar_t));
	mbstowcs(wideFrom, from, strlen(from));
	wchar_t* fromBackup = wideFrom;

	wchar_t* wideTo = calloc(wcslen(wideFrom) +2, sizeof(wchar_t));
	wchar_t *o;
	const wchar_t *end = wideFrom + wcslen(wideFrom);
	int c;

	for (o = wideTo; wideFrom < end; o++) {
		c = *wideFrom++;
		if (c == L'%' && (	!ishex(*wideFrom++)	|| !ishex(*wideFrom++) || !swscanf(wideFrom - 2, L"%2x", &c)))
			break; // On force la sortie de la boucle
		if (wideTo) *o = c;
	}

	// On alloue la taille max que prendra la chaine
	char* to = calloc(wcslen(wideTo) * 4, sizeof(char));
	// On convertit les wide char en char
	wcstombs(to, wideTo, wcslen(wideTo) * 4);
	free(fromBackup);
	free(wideTo);

	// On redimensionne à la taille réelle
	to = realloc(to, (strlen(to) +1) * sizeof(char));
	return to;
}


/**
 * Renvoie le type de requête de façon plus propre
 * (Oui c'est du cache merde j'assume)
 * @author Dorian Gardes
 * @date 04/01/2021
 * @param type La chaine à analyser
 * @return Le type si reconnu. INCONNU sinon.
 */
TypeRequete getTypeRequete(const char* type) {

	// On ne fera pas de commentaire. C'est moche je sais mais on n'as pas vraiment le choix
	if (strcmp(type, "creer") == 0) return CREER;
	else if (strcmp(type, "detruire") == 0) return DETRUIRE;
	else if (strcmp(type, "modifier") == 0) return MODIFIER;
	else if (strcmp(type, "afficher") == 0) return AFFICHER;
	else if (strcmp(type, "motdepasse") == 0) return MOTDEPASSE;
	else return INCONNU;
}

/**
 *
 * @author Dorian Gardes
 * @date 04/01/2021
 * @param req Chaine contenant la requête
 * @param tableau Pointeur vers le tableau défini et rempli par la focntion
 * @return Le nombre d'éléments dans tableau
 */
int decoupageRequete(const char* req, char*** tableau) {

	// On se fait une copie modifiable
	char* requete = calloc(strlen(req) +2, sizeof(char));
	strcpy(requete, req);

	// On commence par retirer le '\n' de fin
	requete[strlen(requete) -1] = '\0';

	int compteCases = 0;
	char* lastPosition = requete;
	// On sort par le break donc non ce n'est pas une boucle infinie
	while (1) {

		char* position = strchr(lastPosition, ' ');
		long len;
		if (position) len = position - lastPosition ;
		else len = (long) strlen(lastPosition);

		// On ajoute une case au tableau
		compteCases++;
        #if defined(WIN32) || defined(__CYGWIN__)
        *tableau = realloc(*tableau, (compteCases +1) * sizeof(char*));
        #else
        *tableau = reallocarray(*tableau, (compteCases +1), sizeof(char*));
        #endif

		char* temp = calloc(len +2, sizeof(char));
		strncpy(temp, lastPosition, len);
		(*tableau)[compteCases-1] = decodeURI(temp);
		free(temp);

		// Le if permet de sortir proprement si la trame est trop courte ou sans arguments
		if (position) lastPosition = position + 1; // +1 pour sauter l'espace
		else break;
	}
	//lastPosition = NULL;
	free(requete);

	return compteCases;
}

/**
 * Vérifie que le nombres d'arguments est cohérent par rapport au type de requete
 * Attention ne vérifie pas le contenu des arguments. En particulier pour les requêtes de type Creer et Afficher qui ont des nbr d'arguments variables.
 * @author Dorian Gardes
 * @date 14/01/2021
 * @param tabRequete Tableau donné par la f(x) découpageRequete()
 * @param lenTabRequete Nombre de cases de tabRequete
 * @return True si le nombre de cases est cohérent. Faux sinon.
 */
bool checkNbrArguments(char** tabRequete, int lenTabRequete) {

	// Une requête sans arguments est obligatoirement composée de 4 champs
	if (lenTabRequete < 4) return false;
	else {
		int nbrArgs = lenTabRequete - 4;
		switch (getTypeRequete(tabRequete[TYPEREQ])) {
			case CREER:
				return isAdmin(tabRequete[USERNAME]) ? (nbrArgs == 2) : (nbrArgs >= 2 && nbrArgs <= 6); // On sort les formules magiques
			case DETRUIRE:
				return isAdmin(tabRequete[USERNAME]) ? (nbrArgs == 1) : (nbrArgs == 2);
			case MODIFIER:
				return nbrArgs == 3;
			case AFFICHER:
				return isAdmin(tabRequete[USERNAME]) ? (nbrArgs == 0) : (nbrArgs == 1 || nbrArgs == 2);
			case MOTDEPASSE:
				return nbrArgs == 1;
			case INCONNU:
			default:
				return true; // On renvoie vrai car le filtrage des commandes inconnues est effectué plus loin dans mainServeur.
		}
	}
}

/**
 * Détermine si le compte est administrateur
 * @author Dorian Gardes
 * @date 02/01/2021
 * @param username
 * @return True si c'est l'administrateur
 */
bool isAdmin(const char* username) {
	return strcmp(username, "admin") == 0;
}

/**
 * Calcule le hash de la chaine.
 * @author Dorian Gardes
 * @date 03/01/2021
 * @param string Chaine à hasher
 * @return Hash corespondant à string
 */
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpointer-sign"
char* sha512(const char* string) {

	// On calcule le hash
	unsigned char hash[SHA512_DIGEST_LENGTH];
	SHA512(string, strlen(string), hash); // Les mots de passes sont stockés hashé avec la fonction SHA512 (Merci OpenSSL)

	// On le rend affichable
	char* printableHash = calloc((2 * SHA512_DIGEST_LENGTH) +1, sizeof(char));
	char currentByte[] = {0, 0, '\0'};
	for(int i = 0; i < SHA512_DIGEST_LENGTH; ++i) {
		sprintf(currentByte, "%02x", hash[i]);
		strcat(printableHash, currentByte);
	}
	return printableHash;
}
#pragma clang diagnostic pop

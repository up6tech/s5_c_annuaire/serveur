//
// Created by Dorian Gardes on 02/01/2021.
//

#ifndef _IOSERVEUR_H
#define _IOSERVEUR_H

#include <stdio.h>

/**************************************************
 * 												  *
 *       CENTRALISATION DES ENTREES/SORTIES       *
 * 												  *
 **************************************************/

#ifdef WIN32
	// On est sur du Microsoft
	#define CHEMIN_BASE "donnees\\"
#else
	// On est sur un système UNIX
	#define CHEMIN_BASE "donnees/"
#endif


/**************************************
 * GESTION ET TRAITEMENT DES REQUETES
 **************************************/

/**
 *
 * @author Dorian Gardes
 * @date 02/01/2021
 * @param codeReponse
 * @param message Charge utile de la réponse.
 * @return 1 si l'envoi s'est bien passé, 0 sinon.
 */
int envoyerReponse(int codeReponse, char* message);

/**
 * Traitement de la requête créer
 * Cette fonction sert principalement d'aiguillage entre le traitement admin et utilisateur standard
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 13/01/2021
 * @param tabRequete Pointeur vers un tableau généré par decoupageRequete()
 * @param lenTabRequete Nbr de cases de tabRequete
 * @return True si compté créé avec succès. Faux sinon.
 */
bool creer(char** tabRequete, int lenTabRequete);

/**
 * Traitement de la requête détruire
 * Cette fonction sert d'aiguillage entre le traitement admin et utilisateur standard
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 13/01/2021
 * @param tabRequete Tableau symbolisant la requête.
 * @return True si compte détruit avec succès. Faux sinon.
 */
bool detruire(char** tabRequete);

/**
 * Traitement de la requête modifier
 * Cette fonction sert d'aiguillage entre le traitement admin et utilisateur standard
 * Si le retour est "false" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 13/01/2021
 * @param username de l'utilisateur courant
 * @param idCompte id du compte/champ à modifier
 * @param nomDuChamp
 * @param nouvelleValeur Valeur à modifier
 * @return True si modification réussie. Faux sinon.
 */
bool modifier(const char* username, const char* idCompte, const char* nomDuChamp, const char* nouvelleValeur);

/**
 * Traitement de la requête afficher
 * Cette fonction sert principalement d'aiguillage entre le traitement admin et utilisateur standard
 * Si le retour est "NULL" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 13/01/2021
 * @param tabRequete Pointeur vers un tableau généré par decoupageRequete()
 * @param lenTabRequete Nbr de cases de tabRequete
 * @return Une chaine correspondant à la réponse de la requête.
 */
char* afficher(char** tabRequete, int lenTabRequete);

/**
 * Change le mot de passe du compte courant
 * Si le retour est "NULL" la commande envoie la réponse au client
 * @author Dorian Gardes
 * @date 04/01/2021
 * @param username de l'utilisateur courant
 * @param nouveauMotDePasse Mot de passe (en clair) à donner au compte admin.
 * @return True si changement réussi. False sinon.
 */
bool editMotDePasse(const char* username, const char* nouveauMotDePasse);

/**
 * Retourne le nom du compte représenté par le
 * @author Dorian Gardes
 * @date 04/01/2021
 * @param numero Numéro de compte donné par afficherComptes()
 * @return Le nom du compte correspondant. Si le numéro est introuvable renvoie NULL;
 */
char* getNomCompteFromNumero(int numero);

/**************************************
 * GESTION DU SYSTEME DE FICHIER
 **************************************/

/**
 * Tente d'ouvrir un fichier et affiche le message d'erreur si nécéssaire
 * @author Dorian Gardes
 * @date 03/01/2021
 * @param nom Nom du fichier à ouvrir
 * @param fichier Pointeur du fichier ouvert
 * @param mode Mode d'ouverture du fichier. Voir fopen()
 * @return True si ouvert avec succès. Faux sinon.
 */
bool ouvrirFichier(const char* nom, FILE** fichier, const char* mode);

/**
 * Donne le chemin relatif pour un nom de fichier donné
 * @author Dorian Gardes
 * @date 03/01/2021
 * @param nom Nom du fichier
 * @return chemin vers ce fichier
 */
char* getCheminFichier(const char* nom);

#endif
